Changes in version 1.0a4:
-------------------------
- bind_itf option is now replaced by the couple bind_address and bind_port
- Corrected some comments
- Removed the line size limit in the config file. An old habit that doesn't seem very useful, here.
- Cleaned up the code of ConfigFile.py, added comments, made several functions where there was only one.
- Added command line options support. Seems to work weel but needs more testing. This is just getting a bit too late for this this evening. We may have some global problems with the way strings are handled. We may want to add an 'vela()' stage, even for string processing, in order to handle string delimiters correctly.


Changes in version 1.0a3:
-------------------------
- I have started replacing the "proxyfy" term by "forward". Dunno how this word had make its way through to my brains, but I'm getting rid of it.
- Modified greatly the configuration file system. Much more simple code now. And more powerful, too. The sample configuration file is slightly different, because of that.
- Corrected a bug in the released PySocks.py that just prevented the server to initialize. Would you believe that because of a typo in a directory name, I had been sure that my alpha releases were working well when they weren't? I wasn't packaging the correct script. I'm getting bored of these mistakes.
- Removed the "connections list" and "binds list" stuff. They were not used anyway, and made it impossible to try other architectures (forking...). If one wants to try a forking server, don't forget to replace all the thread.get_ident() calls in the debugging "print"s.

Changes in version 1.0a2:
-------------------------
- Added 'bind_itf' configuration option
- Improved documentation
- Added the ConfigFile.py module to be able to read options from a configuration file. A sample configuration file is also included, with the few options now working, commented.
- Added the SocketServer2.py file needed to run PySocks. My apologies for having forgotten it in 1.0a1.

Copyright (C) 2001  Xavier Lagraula
See COPYRIGHT.txt and GPL.txt for copyrights information.

Changes in version 1.0a1:
-------------------------
Initial release.

Copyright (C) 2001  Xavier Lagraula
See COPYRIGHT.txt and GPL.txt for copyrights information.
