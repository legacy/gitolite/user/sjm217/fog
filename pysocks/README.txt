PySocks - a platform independent SOCKS proxy server written in Python

README information.

This is the first distribution package for PySocks, the SOCKS proxy server written in Python.

The version number of this package is 1.0a1 (first alpha release of the first version).

I strongly encourage you to read this file and all the other text files provided in this package before going any further with using this software.

This software is in a pre-alpha stage: it appears to run correctly, BUT:
- it has only been tested on my computer for now (Windows 2000),
- it lacks many important features in the user interface domain,
- it lacks many even more important features in the security domain,
- it has not been tested with prior versions of Python,
- it has nothing that could honestly call documentation, even if I have made many doc strings and comments in the source code,
- it doesn't netively support multi-homed servers.

So don't forget to have a look at the "konwn problems" section at the end of this document.

WHAT IS A SOCKS PROXY?
----------------------

With the apparition of firewalls and Internet connection sharing, some problems have appeared in using softwares that require that the computer of the user accept incoming connection from the Internet. Firewalls blocks them for security reason, and connection sharing makes them impossible by design. One could not use ICQ, IRC to do file transfers, FTP couldn't work anymore except in a special mode ("passive" mode) which is not always supported, etc.

The SOCKS protocol is designed to correct this situation. Running a socks proxy server on the computer running the firewall software or sharing the connection allows one to bypass the "incoming connection problem", as long as the client application (ICQ, IRC, ...) knows how to use it.

WHAT IS PYSOCKS?
----------------

There are 2 versions of the SOCKS protocol: SOCKS 4 and SOCKS 5. PySocks implements SOCKS 4 which is must more simple, but powerful enough for its first purpose. Once PySocks gets out of the beta stage, the net step will be to extend it to support the SOCKS 5 protocol.
As for now, the v4 protocol is fully supported, except for one point dealing with a time limit, and the extension provided by the v4a version of the protocol (see TODO.txt).

WHAT DO ONE NEED TO RUN PYSOCKS?
--------------------------------

If you're a home user sharing an Internet connection, you need to install Python 2.1 on the computers that does the sharing stuff. If you're a security administrator in charge of a firewall, then you need to install Python 2.1 on the computer running the firewall software and configure this software appropriately. But in that second case, I wouldn't suggest using PySocks at all: at this stage of developement of PySocks, it would be MUCH safer to use some commercial product.

See INSTALL.txt for more detailed isntructions.

CONTENT OF THE PACKAGE
----------------------

This package contains 14 files:
- PySocks.py        : the SOCKS proxy server script
- IPv4_Tools.py     : some IPv4 helper functions
- IDENT_Client.py   : some IDENTD helper functions
- SockeServer2.py   : a patched version of SocketServer.py from Python v2.1
- README.txt        : you're reading it
- INSTALL.txt       : installation instructions
- TODO.txt          : things that are to be done. There are a lot of them. This is the first pre-alpha release, after all
- COPYRIGHT.txt     : copyright information
- GPL.txt           : the GNU general public license
- MD5.txt           : the MD5 digests of all above files
- Documentation/rfc1413.txt             : reference document (IDENT protocol)
- Documentation/rfc1918.txt             : reference document (IP addressing considerations)
- Documentation/socks4.protocol.txt     : reference document (SOCKS 4 protocol definition)
- Documentation/socks4a.protocol.txt    : reference document (SOCKS 4 protocol extension)

Note: PySocks.py may be safely imported in an interactive Python session or another script without freezing it, if you want to play with it.

KNOWN PROBLEMS
--------------

- I don't know yet how PySocks behaves when launched as a daemon under UN*X or a service under Windows NT/2000. This is not my priority: once stable and reliable, I'll start to think about running it silently in the "usual" ways for a linux daemon or a NT service.

- This software is still in development stage, and because of the lack of a platform independant loggin facility, PySocks prints lots of nasty stuff on its standard output. Not quite enough to make its unreadable, though, and it allows one to have a look at what's happening. If one wants to get rid of it, one can remove ALL "print" statement in the *.py files. None are necessary for now.

- No authorization rules system is built for now, and there is no support for multi-homed servrs, so a few choices had been made for security's sake: no request may come from an host using a routable address (RFC 1918), and requests are forwarded onto the first interface of the proxy server that has a routable address. Beware: this also means that one can not use the proxy to connect to a local server (request are forwarded to the Internet interface of the proxy server).

- Well, this is not a PySocks bug, but Some socket closed by a peer (client or remote) are not detected and used to finish in an infinite loop: select() always returned the "request" socket as ready to be read even if there is nothing more to read on it. This situation is now detected and lead to a "Closed_Connection" exception, thus closing this connection properly. I don't know if this a common phenomenon when dealing with sockets, or if it is special to Python or the Windows environment, but I'd rather not have to use this kind of workaround.

- There is a lethal bug in the threading TCP server class I used as a base for this proxy server, in Python 2.1. I ship with this release a patched version of SocketServer.py BUT this is nothing "official" so the file has a different name (just added a "2" at the end :). I'm waiting for the time when I'll be able to tell "PySocks need the patch foo be applied".

- Under Windows, once launched, PySocks CANNOT be interrupted by any keyboard action: one needs to use the task manager or to close the window used to launch it. This is inherent to the base server class provided in SocketServer.py and Windows and there isn't much I can do about this.

- PySocks must be launched AFTER the computer had been connected to the Internet.

THANKS
------

My thanks go to Wendy who gave me the idea, the courage and the excuse to waste 3 week ends learning Python, writing the first version of this software and to release it as my first open source public project. Hi Wendy! And no, Wendy, I will not take the time to re-write all this in french. I already had the courage to write all this in english to release it, so you'll have to make the effort to read it as it is...

FEEDBACK
--------

Any comment, suggestion, request features, bug reports and such can be done either using the SourceForge website or by writing directly to pysocks-admin@netcourrier.com. Preferably the first solution.


This product is licensed under the GNU general public license (GPL), so if any of these file is missing (especially the GPL), has an incorrect MD5 digest, or if any of the files named in COPYRIGHT.txt lacks the copyright notice, I suggest reporting it to GNU as told in the GPL, and sending me an email.

Copyright (C) 2001  Xavier Lagraula
See COPYRIGHT.txt and GPL.txt for copyrights information.
