    PySocks - a platform independent SOCKS proxy server written in Python
    Copyright (C) 2001  Xavier Lagraula

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

The GNU general public license version 2 (GPL) can be found in file GPL.txt that is part of this package. If the package you have doesn't contain it and/or any of copyright notices in the files of this package had been removed, then you should look at it with suspicion. In such a case, I encourage you to get the latest version of PySocks on the sourceforge site ( http://www.sourceforge.net ).

The files under protection of the GPL in this package are:

- IDENT_Client.py
- IPv4_Tools.py
- PySocks.py

- README.txt
- INSTALL.txt
- TODO.txt
- CHANGES.txt

- sample_config.conf

I can be joined by electronic mail at pysocks-admin@netcourrier.com, or by paper mail:
M. Xavier Lagraula
211 Avenue de Versailles
75016 Paris (France)