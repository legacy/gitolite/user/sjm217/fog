def obfuscate(data, key):
    out = []
    for i, c in enumerate(data):
        keybyte = key[i % len(key)]
        out.append(chr(ord(c) ^ ord(keybyte)))
    return "".join(out)

def format_headers(headers):
    return str(headers).replace('\r\n', '/').replace('\n', '').replace('\r','')

class HTTPObfuscator(object):
    def __init__(self, sock):
        self.sock = sock
        self.rfile = self.wfile = self.sock.makefile()

    def send_header(self, keyword, value):
        self.send("%s: %s\r\n" % (keyword, value))

    def deobfuscate(self, data):
        return obfuscate(data, self.key)

    def obfuscate(self, data):
        return obfuscate(data, self.key)

    def send(self, data):
        self.wfile.write(data)

    def end_headers(self):
        self.send('\r\n')

    def flush(self):
        self.wfile.flush()

    def close(self):
        self.wfile.close()
