Usage
=====

There are two components, one which runs on the same host as the client (OP):
HTTor_OP, and one on the same host as the bridge (OR): HTTor_OR.

On the client
-------------

First, configure Tor to use a bridge, e.g. add to torrc:
 Bridge 192.0.2.11:9001
 UseBridges 1
and to use a SOCKS proxy (HTTor_OP), e.g. add to torrc:
 Socks4Proxy 127.0.0.1:1080

Start HTTor_OP, specifying the address:port of the bridge that Tor will
try to connect to (the same as was set in the torrc, e.g 192.0.2.11:9001), and
the address:port that HTTP_OR.py is listening on (e.g. 192.0.2.11:8080)
 $ python HTTor_OP --special_ips=192.0.2.11:9001:192.0.2.11:8080

On the bridge
-------------

Start HTTP_OR.py, specifying the port to listen on and the address:port
of the bridge's OR port, e.g.
 $ python HTTor_OR 8080 127.0.0.1:9001

Debugging
---------

To help debugging, the Tor OP can be replaced by:
 socat STDIO SOCKS4:localhost:192.0.2.11:9001
and the Tor OR can be replaced by:
 socat TCP4-LISTEN:9001,reuseaddr STDIO
Tor can be tested using:
 curl --socks4a 127.0.0.1:9060 -o - http://check.torproject.org/

Credits
=======

The code in pysocks is adapted from the PySocks project
<http://sourceforge.net/projects/pysocks/> by Xavier Lagraula.
It is licenceced under the GPL v2 (or later). See pysocks/COPYRIGHT.txt for
more information.
The HTTP parsing code is partly based on httplib.py and BaseHTTPServer from
Python.
